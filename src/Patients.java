import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class Patients
{
   private ArrayList<Patient> patients = new ArrayList<>();

   ////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////
   public void addPatient( Patient patient )
   {
      patients.add( patient );
   }

   ////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////
   public void write()
   {
      int index = 0;
      for (var p : patients)
      {
         System.out.printf( "\npatient %2d.\n", ++index );
         p.print();
      }
   }

   ////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////
   public void save( String filename ) throws FileNotFoundException, UnsupportedEncodingException
   {
      PrintWriter writer = new PrintWriter( filename, "UTF-8" );

      for (var patient : patients)
      {
         writer.println( patient.toJSON() );
      }

      writer.flush();
      writer.close();
   }

   ////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////
   public void load( String filename ) throws FileNotFoundException
   {
      patients.clear();

      File file = new File( filename );
      if (!file.exists())
      {
         System.out.format( "ERROR: file %s does not exist.\n", filename );
         System.exit( 0 );
      }
      InputStream is = new FileInputStream( file );
      //InputStream is = Patients.class.getResourceAsStream( filename );
      JSONTokener tokener = new JSONTokener( is );

      while (true)
      {
         try
         {
            JSONObject object     = new JSONObject( tokener );
            Patient    newPatient = new Patient( object );
//            newPatient.print();
            System.out.println( "====================" );
            patients.add( newPatient );
         }
         catch (org.json.JSONException e)
         {
            System.out.println( "end-of-input" );
            break;
         }
         catch (Exception e)
         {
            e.printStackTrace();
         }
      }
   }
}
