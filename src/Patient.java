import org.json.JSONArray;
import org.json.JSONObject;

import java.time.LocalDateTime;
import java.util.ArrayList;

////////////////////////////////////////////////////////////////////////////////
// Class representing a Patient. Create to demostrate json read/write
// Although still very simple, the class is not trivial.
//    - there is a string member, an int member as well as an array of Objects.
////////////////////////////////////////////////////////////////////////////////
class Patient
{
   private String                       name;
   private int                          uid;
   private ArrayList<WeightMeasurement> weightMeasurements = new ArrayList<WeightMeasurement>();

   ////////////////////////////////////////////////////////////////////////////////
   /// CTOR: construct an object from name + id.
   ////////////////////////////////////////////////////////////////////////////////
   public Patient( String name, int id )
   {
      this.name = name;
      uid       = id;
   }

   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public void addWeight( LocalDateTime dateTime, double mass )
   {
      weightMeasurements.add( new WeightMeasurement( dateTime, mass ) );
   }

   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public void addWeight( LocalDateTime dateTime, double mass, String description )
   {
      weightMeasurements.add( new WeightMeasurement( dateTime, mass, description ) );
   }

   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public void print()
   {
      System.out.format( "naam: %s: id=%d\n", name, uid );
      for (WeightMeasurement w : weightMeasurements)
      {
         w.print();
      }
   }

   ////////////////////////////////////////////////////////////////////////////////
   /// CTOR: Construct an object from a JSONObject
   ////////////////////////////////////////////////////////////////////////////////
   public Patient( JSONObject object )
   {
      name = object.getString( "name" );
      uid  = object.getInt( "uid" );

      JSONArray weights = object.getJSONArray( "weightMeasurements" );
      for (int i = 0; i < weights.length(); i++)
      {
         WeightMeasurement w = new WeightMeasurement( (weights.getJSONObject( i )) );
         this.weightMeasurements.add( w );
      }
   }

   ////////////////////////////////////////////////////////////////////////////////
   /// Serialize an object to a JSONObject.
   ////////////////////////////////////////////////////////////////////////////////
   public JSONObject toJSON()
   {
      JSONObject jobj = new JSONObject();

      jobj.put( "name", this.name );
      jobj.put( "uid", this.uid );

      JSONArray ja = new JSONArray();
      for (var w : weightMeasurements)
      {
         ja.put( w.toJSON() );
      }
      jobj.put( "weightMeasurements", ja );

      return jobj;
   }
}
